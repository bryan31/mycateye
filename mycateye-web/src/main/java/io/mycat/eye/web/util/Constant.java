/**  
 * All rights Reserved, Designed By www.mycat.io
 * @Title:  Constant.java   
 * @Package cn.bqjr.mysqleye.util   
 * @Description:    TODO 
 * @author: 李平(360841519@qq.com)    
 * @date:   2017年3月21日 下午4:59:26   
 * @version V1.0 
 * @Copyright: 2017 www.mycat.io Inc. All rights reserved. 
 */
package io.mycat.eye.web.util;

/**   
 * @ClassName:  Constant   
 * @Description:常量
 * @author: 李平(360841519@qq.com)
 * @date:   2017年3月21日 下午4:59:26   
 *     
 * @Copyright: 2017 www.mycat.io Inc. All rights reserved. 
 */
public class Constant {
	public static final String ZERO = "0";
	public static final String ERROR = "error";
	
	public static final String STATUS_UNKNOWN = "未知";
}
