package io.mycat.eye.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MycateyeAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(MycateyeAdminApplication.class, args);
	}
}
